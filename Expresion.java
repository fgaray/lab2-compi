import java.util.*;


enum Operacion{
        Suma,
        Resta,
        Multiplicacion,
        Division,

        //Son expresiones separadaasSon expresiones separadaas
        Ninguna
}




class Expresion{
        
        // Ej: exp1 + exp2
        Expresion exp1, exp2;

        String elemento;

        Simbolo s;

        String tipo_dato;

        Operacion op;

        int fila, columna;


        Expresion(String elemento, String tipo_dato)
        {
                this.exp1 = null;
                this.exp2 = null;

                this.elemento = elemento;
                this.tipo_dato = tipo_dato;
        }


        Expresion(Expresion exp1, Expresion exp2, Operacion op)
        {
                this.exp1 = exp1;
                this.exp2 = exp2;

                this.op = op;
        }



        /**Devuelve la cantidad de expresiones no relaciondas que hay en esta
         * expresion. No realacionadas en el senditod de separadas por comas:
         * exp1, exp2, exp3, etc*/
        int tamanoNivel1()
        {
                return 1+this.tamanoNivel1(this);
        }



        private int tamanoNivel1(Expresion x)
        {
                if(x == null)
                {
                        return 0;
                }else{
                        if(x.op == Operacion.Ninguna)
                        {
                                return 1 + this.tamanoNivel1(x.exp1) +  this.tamanoNivel1(x.exp2);
                        }else{
                                return this.tamanoNivel1(x.exp1) +  this.tamanoNivel1(x.exp2);
                        }
                }

        }


        /**Devuelve un vector de expresiones. Cada elemento del vector contiene
         * una clase expresion que puede tener otras expresiones como nodos*/

        public Vector<Expresion> expresiones_argumentos()
        {
                Vector<Expresion> v = new Vector<Expresion>();

                expresiones_argumentos(this, v);

                return v;
        }


        private void expresiones_argumentos(Expresion e, Vector<Expresion> v)
        {
                
                if(e.op == Operacion.Ninguna)
                {
                        //quiere decir que tenenmos 2 expresiones validas ahora
                        expresiones_argumentos(e.exp1, v);
                        expresiones_argumentos(e.exp2, v);

                }else{
                        v.add(e);
                }


        }

        public String evaluar_tipo_dato(TablaSimbolos tabla, String f)throws Exception
        {
                return this.evaluar_tipo_dato(this, tabla, f);
        }

        public String evaluar_tipo_dato()throws Exception
        {
                return this.evaluar_tipo_dato(this, null, null);
        }

        private String evaluar_tipo_dato(Expresion e, TablaSimbolos tabla, String f)throws Exception
        {
                if(e.tipo_dato != null && e.exp1 == null && e.exp2 == null)
                {
                        return e.tipo_dato;
                }else{

                        if(e.s != null && tabla != null && f != null)
                        {
                                //buscamos el simbolo en la TablaSimbolos
                                Simbolo s = tabla.obtenerSimbolo(f, e.s.id);

                                if(s != null)
                                {
                                        //da null?
                                        return s.tipo_dato;
                                }else{
                                        //variable no definida dentro de la
                                        //funcion, por lo tanto debemos ver si
                                        //esta definina globalmente

                                        s = tabla.obtenerSimbolo(e.s.id);
                                        if(s == null)
                                        {
                                                throw new VariableNoDeclarada(e.s);
                                        }else{
                                                return s.tipo_dato;
                                        }
                                }
                        }else{

                                if(e.exp1 == null && e.exp2 != null)
                                {
                                        return this.evaluar_tipo_dato(e.exp2, tabla, f);

                                }else if(e.exp1 != null && e.exp2 == null){
                                        return this.evaluar_tipo_dato(e.exp1, tabla, f);

                                }else if(e.exp1 == null && e.exp2 == null){
                                        return e.tipo_dato;
                                }else{

                                        return deducir_tipo_dato(this.evaluar_tipo_dato(e.exp1, tabla, f), this.evaluar_tipo_dato(e.exp2, tabla, f));
                                }
                        }
                }

        }


        private String deducir_tipo_dato(String t1, String t2)
        {
                if(t1.equals("int") && t2.equals("int"))
                {
                        return "int";
                }else if(t1.equals("float") || t2.equals("float") && !t1.equals("void") && !t2.equals("void")){
                        return "float";
                }else{
                        return "void";
                }
        }


        private Vector<Expresion> unir_vectores(Vector<Expresion> v1, Vector<Expresion> v2)
        {
                Vector<Expresion> nv = new Vector<Expresion>();

                for(int i = 0;i<v1.size();i++)
                {
                        nv.add(v1.elementAt(i));
                }

                for(int i = 0;i<v2.size();i++)
                {
                        nv.add(v2.elementAt(i));
                }

                return nv;
        }

}

class VariableNoDeclarada extends Exception{

        Simbolo s;

        VariableNoDeclarada(Simbolo s)
        {
                this.s = s;
        }

}
