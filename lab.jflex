/*CODIGO JFLEX*/
/*AREA DE IMPORTACION*/
import java.io.*;
import java_cup.runtime.*;

/*CLASE DATOS*/

class Datos{
	public static String nombreArchivo = ""; //contendra el nombre del codigo que se esta revizando	
	public static String palabra = ""; //el token que se esta revizando
	public static String codigoCorrecto = ""; //el codigo para la impresion
	public static int fila = 1; //fila actual
	public static int columna = 1; //columna actual
        public static int lineas = 0;
        public static int errores_lexicos = 0;
        public static int errores_sintacticos = 0;
        public static int errores_semanticos = 0;

	//metodo que escribe la salida en un archivo e texto: "entrada.txt"
	public static void escribe(String archivo, String textos){
		try{          
				FileWriter file1 = new FileWriter(archivo+".txt");

				BufferedWriter buff = new BufferedWriter(file1);

				buff.write(textos);
				buff.newLine();

				buff.close();
				file1.close();

                                if(errores_lexicos == 0 && errores_sintacticos == 0 && errores_semanticos == 0)
                                {
                                        System.out.println("No hay errores.");
                                }else{
                                        
                                        if(errores_lexicos > 0)
                                        {
                                                System.out.println(errores_lexicos + " errores lexicos encontrados");
                                        }

                                        if(errores_sintacticos > 0)
                                        {
                                                System.out.println(errores_sintacticos + " errores sintáctico encontrados");
                                        }

                                        if(errores_semanticos > 0)
                                        {
                                                System.out.println(errores_semanticos + " errores semánticos encontrados");
                                        }

                                }
                                
                                System.out.println("Se imprimio el codigo correcto en: "+archivo+".txt");
		}
		catch(IOException e){
				System.out.println("<"+archivo+"> Error: no se puede escribir");
		}
	}

        public static void manejoLexema(String yytext, int yyline, int yycolumn)
        {
                lineas = 0;

                if(!palabra.equals(""))
                {
                        codigoCorrecto += palabra+" ";	
                }

                palabra = yytext;
                fila = yyline;
                columna = yycolumn;

        }
}

%%

%line
%column
%cup
%unicode

%{
        Datos datos = new Datos();
%}

%eof{
	//datos.codigoCorrecto += "\n";	
%eof}

/*Expresiones regulares para otras cosas*/


/*expresiones regulares del lab*/

digito = [0-9]
numero = {digito}+ | {digito}+ "."{digito}+
letra = [a-zA-Z]
id = {letra} ({letra} | {numero})*
oprel = "= =" | "!=" | "<" | "<=" | ">=" | ">" | "&&" | "||"
opsuma = "+" | "-"
opmult = "*" | "/"
id_lib = "<"{id}".h"">"
blancos = " " | "\t"
todo = [^]*
comentario = "/*"{todo}"*/" | "//"[^\n]*"\n"
malcomentado   = "/*" .*"/*" .*"*/"|"/*"+"*".*   

/*AREA DE REGLAS*/
%%


<YYINITIAL>
{
        {comentario} {
                //comentarioa, no hacemos nada
                datos.lineas = 0;
                datos.fila = yyline;
                datos.columna = yycolumn;
        }

        {malcomentado} {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                //System.out.println("<"+datos.nombreArchivo+">:Error Sintactico ("+datos.fila+","+datos.columna+"): comentario mal definido."); 

        }

        "'" {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.COMILLA, new FSymbol(yytext(), yyline, yycolumn));
        }
		
        "=" {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.IGUAL, new FSymbol(yytext(), yyline, yycolumn));
        }
		
        "{" {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.LLAVEI, new FSymbol(yytext(), yyline, yycolumn));
        }
		
        "}" {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.LLAVED, new FSymbol(yytext(), yyline, yycolumn));
        }


        "[" {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.CORI, new FSymbol(yytext(), yyline, yycolumn));
        }

        "]" {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.CORD, new FSymbol(yytext(), yyline, yycolumn));
        }
		
        "(" {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.PARI, new FSymbol(yytext(), yyline, yycolumn));
        }
		
        ")" {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.PARD, new FSymbol(yytext(), yyline, yycolumn));
        }
		
        "#" {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.GATO, new FSymbol(yytext(), yyline, yycolumn));
        }
		
        "," {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.COMA, new FSymbol(yytext(), yyline, yycolumn));
        }
		
        ";" {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.TDOT, new FSymbol(yytext(), yyline, yycolumn));
        }
		
        "return" {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.RETURN, new FSymbol(yytext(), yyline, yycolumn));
        }
		
        "while" {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.WHILE, new FSymbol(yytext(), yyline, yycolumn));
        }
		
        "do" {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.DO, new FSymbol(yytext(), yyline, yycolumn));
        }
		
        "else" {
                datos.manejoLexema(yytext(), yyline, yycolumn); 
                return new Symbol(sym.ELSE, new FSymbol(yytext(), yyline, yycolumn));
        }
		
        "if" {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.IF, new FSymbol(yytext(), yyline, yycolumn));
        }
		
        "void" {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.VOID, new FSymbol(yytext(), yyline, yycolumn));
        }
		
        "char" {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.CHAR, new FSymbol(yytext(), yyline, yycolumn));
        }
		
        "int" {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.INT, new FSymbol(yytext(), yyline, yycolumn));
        }
		
        "float" {
                datos.manejoLexema(yytext(), yyline, yycolumn); 
                return new Symbol(sym.FLOAT, new FSymbol(yytext(), yyline, yycolumn));
        }
		
        "include" {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.INCLUDE, new FSymbol(yytext(), yyline, yycolumn));
        }

        {oprel} {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.OPREL, new FSymbol(yytext(), yyline, yycolumn));
        }



		
        {numero} {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.NUMERO, new FSymbol(yytext(), yyline, yycolumn));
        }
		
		
        {id} {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.ID, new FSymbol(yytext(), yyline, yycolumn));
        }
		
		
        {opsuma} {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.OPSUMA, new FSymbol(yytext(), yyline, yycolumn));
        }
		
        {opmult} {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.OPMULT, new FSymbol(yytext(), yyline, yycolumn)); }
		
        {id_lib} {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.ID_LIB, new FSymbol(yytext(), yyline, yycolumn));
        }

        {blancos} {
                //un espacio, no hacemos nada
                datos.lineas = 0;
                datos.fila = yyline;
                datos.columna = yycolumn;
        }



        "\n" {
                //salto de linea
                if(datos.lineas ==  0)
                {
                        datos.codigoCorrecto += datos.palabra+" ";	
                        datos.palabra = "\n";
                }

                datos.lineas++;
                datos.fila = yyline;
                datos.columna = yycolumn;
        }



}

[^] {	
        System.out.println("<"+datos.nombreArchivo+"> Error Léxico ("+yyline+","+yycolumn+"): Lexema: <"+yytext()+"> no válido");

        datos.errores_lexicos++;
}
	
