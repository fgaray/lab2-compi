all:
	java -jar JFlex.jar lab.jflex
	java java_cup.Main lab.cup
	javac *.java

install: all
	jar cfe compilador.jar parser *.class  java_cup/* 
	cp compilador.jar /bin
	cp scripts/compilador /bin
	chmod +x /bin/compilador


uninstall:
	rm /bin/compilador
	rm /bin/compilador.jar

