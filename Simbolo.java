import java.util.Vector;

enum Tipo{
        Funcion,
        Variable
}




class Simbolo{
        
        Tipo tipo;
        String tipo_dato;
        String id;
        int fila, columna;



        Simbolo(String id, Tipo tipo, String tipo_dato)
        {
                this.id = id;
                this.tipo = tipo;
                this.tipo_dato = tipo_dato;
        }

        Simbolo(String id, Tipo tipo)
        {
                this.id = id;
                this.tipo = tipo;
        }


        Simbolo copiar()
        {
                Simbolo s = new Simbolo(this.id, this.tipo, this.tipo_dato);

                s.fila = this.fila;
                s.columna = this.columna;

                return s;
        }

}

class FuncionDefinicion extends Simbolo{

        Vector<Simbolo> argumentos;
        Vector<Proposicion> proposiciones;



        FuncionDefinicion(String id, Tipo tipo, String tipo_dato)
        {
                super(id, tipo, tipo_dato);
        }

        FuncionDefinicion(String id, Tipo tipo)
        {
                super(id, tipo);
        }


        void agregarArgumentos(Vector<Simbolo> s)
        {
                this.argumentos = s;
        }

        void agregarProposiciones(Vector<Proposicion> p)
        {
                this.proposiciones = p;
        }

}
