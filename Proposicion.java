import java.util.Vector;


enum TipoProposicion{
        Variable,
        Funcion,
        Return,
        DeclaracionIf,
        DeclaracionWhile
}


class Proposicion{

        public int fila, columna;
        public TipoProposicion tipo_proposicion;

        String id;


}


class Variable extends Proposicion{
        public String id_variable;
        public Expresion exp;
        public Simbolo s;

        Variable(String id_variable, Expresion exp, Simbolo s)
        {
                this.id_variable = id_variable;
                this.exp = exp;
                this.s = s;

                this.tipo_proposicion = TipoProposicion.Variable;
        }
}


class Funcion extends Proposicion{

        public String id_funcion;

        Funcion()
        {
                this.tipo_proposicion = TipoProposicion.Funcion;
        }

}

class Return extends Proposicion{

        Expresion exp;


        Return(Expresion e)
        {
                this.tipo_proposicion = TipoProposicion.Return;
                this.exp = e;
        }

}

class DeclaracionIf extends Proposicion{

        Expresion exp;
        Vector<Proposicion> vp;

        DeclaracionIf(Expresion exp, Vector<Proposicion> vp)
        {
                this.exp = exp;
                this.vp = vp;

                this.tipo_proposicion = TipoProposicion.DeclaracionIf;
        }

}

class DeclaracionWhile extends Proposicion{
        Expresion exp;
        Vector<Proposicion> vp;

        DeclaracionWhile(Expresion exp, Vector<Proposicion> vp)
        {
                this.exp = exp;
                this.vp = vp;

                this.tipo_proposicion = TipoProposicion.DeclaracionWhile;
        }

}
