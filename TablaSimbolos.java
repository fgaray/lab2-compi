import java.util.Hashtable;
import java.util.Vector;


class TablaSimbolos{
        /**En esta clase administramos los simbolos que se han ido definiendo en
         * el programa*/



        /**Simbolos declarados de forma global, como funciones o variables
         * globales*/
        Vector<Simbolo> simbolos_globales;



        /**Simbolos declarados en las funciones.
         * Seria Hashtable<NOMBRE_FUNCION, Simbolos_declarados_funcion>*/
        Hashtable<String, Vector<Simbolo>> simbolos_funciones;


        TablaSimbolos()
        {
                this.simbolos_globales = new Vector<Simbolo>();
                this.simbolos_funciones = new Hashtable<String, Vector<Simbolo>>();

        }



        //registro de simbolos

        
        //registra un simnbolo global para despues verificar que sea unico
        void registrar(Simbolo simbolo)
        {
                this.simbolos_globales.add(simbolo);
        }


        //registra un simbolo que pertenece a la funcion
        void registrar(String funcion, Simbolo simbolo)
        {
                Vector<Simbolo> v = this.simbolos_funciones.get(funcion);

                if(v != null)
                {
                        v.add(simbolo);
                }else{
                        v = new Vector<Simbolo>();

                        v.add(simbolo);

                        this.simbolos_funciones.put(funcion, v);
                }
        }


        //Funciones que verifican si existen los simbolos

        //devuelve verdadero si el simbolo ya existe en la funcion
        boolean existe(String nombre_funcion, Simbolo simbolo)
        {
                Vector<Simbolo> v = this.simbolos_funciones.get(nombre_funcion);

                return this.existe(simbolo, v);
        }


        //devuelve verdader si el simbolo existe en las declaraciones globales
        boolean existe(Simbolo simbolo)
        {
                return this.existe(simbolo, this.simbolos_globales);
        }



        boolean existeIgnorarTipo(Simbolo simbolo)
        {
                Vector<Simbolo> v = this.simbolos_globales;


                for(int i = 0;i<v.size();i++)
                {
                        Simbolo vs = v.elementAt(i);

                        if(vs.id.equals(simbolo.id))
                        {

                                return true;
                        }
                }


                return false;

        }



        Simbolo obtenerSimbolo(String id)
        {
                Vector<Simbolo> v = this.simbolos_globales;


                for(int i = 0;i<v.size();i++)
                {
                        Simbolo s = v.elementAt(i);

                        if(s.id.equals(id))
                        {
                                return s;
                        }
                }


                return null;
        }

        Simbolo obtenerSimbolo(String f, String id)
        {
                Vector<Simbolo> v = this.simbolos_funciones.get(f);


                if(v != null)
                {
                        for(int i = 0;i<v.size();i++)
                        {
                                Simbolo s = v.elementAt(i);

                                if(s.id.equals(id))
                                {
                                        return s;
                                }
                        }
                }


                return null;
        }


        private boolean existe(Simbolo s, Vector<Simbolo> v)
        {

                if(v != null && s != null)
                {
                        for(int i = 0;i<v.size();i++)
                        {
                                Simbolo vs = v.elementAt(i);

                                if(vs.tipo == s.tipo)
                                {
                                        if(vs.id.equals(s.id))
                                        {
                                                return true;
                                        }
                                }
                        }
                }

                return false;
        }
}
