import java_cup.runtime.*;


class FSymbol{

        private int fila, columna;
        String texto;



        FSymbol(String texto, int fila, int columna)
        {
                this.texto = texto;

                this.fila = fila+1;
                this.columna = columna+1;
        }


        static String s(Object f)
        {
                return ((FSymbol)f).texto;
        }

        static int f(Object f)
        {
                return ((FSymbol)f).fila;
        }

        static int c(Object f)
        {
                return ((FSymbol)f).columna;
        }
}
