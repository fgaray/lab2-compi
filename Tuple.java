class Tuple<X, Y>{

        private final X x;
        private final Y y;

        Tuple(X x, Y y)
        {
                this.x = x;
                this.y = y;
        }


        public X fst()
        {
                return this.x;
        }

        public Y snd()
        {
                return this.y;
        }
}
